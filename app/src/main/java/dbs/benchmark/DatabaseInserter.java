package dbs.benchmark;


import java.sql.SQLException;

import static dbs.benchmark.databases.cassandra.CassandraBenchmark.insertDataToCassandra;
import static dbs.benchmark.databases.file.File.insertDataToFile;
import static dbs.benchmark.databases.postgres.PostgresBenchmark.insertDataToPostgreSQL;
import static dbs.benchmark.databases.postgres.PostgresFromFileBenchmark.insertDataToPostgreSQLFromFile;
import static dbs.benchmark.databases.shared.Shared.deleteFileByName;

public class DatabaseInserter {

    private static final int AMOUNT_OF_USERS = 10_000_000;

    public void insertData() throws SQLException {
        final String fileName = "users.csv";


        //insertDataToOracle(AMOUNT_OF_USERS);
//        insertDataToMySQL(AMOUNT_OF_USERS);
//        insertDataToPostgreSQL(AMOUNT_OF_USERS);
//        insertDataToCassandra(AMOUNT_OF_USERS);
        insertDataToFile(AMOUNT_OF_USERS, fileName);
        insertDataToPostgreSQLFromFile(AMOUNT_OF_USERS, fileName);


        // Clean up
        deleteFileByName(fileName);

    }

}

