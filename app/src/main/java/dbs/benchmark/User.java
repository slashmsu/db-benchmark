package dbs.benchmark;

import java.time.LocalDate;

public class User {
    private String username;
    private String firstname;
    private String secondname;
    private LocalDate dateofbirthday;
    private int age;
    private String password;

    public User() {

    }

    public User(String username, String firstname, String secondname, LocalDate dateofbirthday, int age, String password) {
        this.username = username;
        this.firstname = firstname;
        this.secondname = secondname;
        this.dateofbirthday = dateofbirthday;
        this.age = age;
        this.password = password;
    }

    // Getters and setters
    public String getUsername() {
        return username;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getSecondname() {
        return secondname;
    }

    public LocalDate getDateofbirthday() {
        return dateofbirthday;
    }

    public int getAge() {
        return age;
    }

    public String getPassword() {
        return password;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public void setSecondname(String secondname) {
        this.secondname = secondname;
    }

    public void setDateofbirthday(LocalDate dateofbirthday) {
        this.dateofbirthday = dateofbirthday;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}

