package dbs.benchmark.databases.cassandra;

import com.datastax.oss.driver.api.core.CqlSession;
import com.datastax.oss.driver.api.core.cql.BatchStatement;
import com.datastax.oss.driver.api.core.cql.BatchStatementBuilder;
import com.datastax.oss.driver.api.core.cql.BatchType;
import com.datastax.oss.driver.api.core.cql.SimpleStatement;
import dbs.benchmark.User;

import java.net.InetSocketAddress;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.UUID;

import static dbs.benchmark.databases.shared.Shared.generateRandomUser;
import static dbs.benchmark.databases.shared.Shared.logResult;

public class CassandraBenchmark {

    public static void insertDataToCassandra(final int amountOfUsers) {

        // Create a temporary session without a keyspace
        CqlSession tempSession = CqlSession.builder()
                .addContactPoint(new InetSocketAddress("127.0.0.1", 9042))
                .withLocalDatacenter("datacenter1")
                .build();

        // Create the keyspace if it does not exist
        String createKeyspaceCql = "CREATE KEYSPACE IF NOT EXISTS testdb WITH replication = {'class': 'SimpleStrategy', 'replication_factor': '1'}";
        tempSession.execute(createKeyspaceCql);

        // Close the temporary session
        tempSession.close();

        // Now create the session with the keyspace
        CqlSession session = CqlSession.builder()
                .addContactPoint(new InetSocketAddress("127.0.0.1", 9042))
                .withKeyspace("testdb")
                .withLocalDatacenter("datacenter1")
                .build();

        // Create user table if not exists
        String cql = "CREATE TABLE IF NOT EXISTS users (id UUID PRIMARY KEY, username TEXT, firstname TEXT, secondname TEXT, dateofbirthday DATE, age INT, userpassword TEXT)";
        SimpleStatement statement = SimpleStatement.newInstance(cql);
        session.execute(statement);

        String cqlInsert = "INSERT INTO users (id, username, firstname, secondname, dateofbirthday, age, userpassword) VALUES (?, ?, ?, ?, ?, ?, ?)";
        SimpleStatement insertStatement;
        Instant start = Instant.now();

        int batchSize = 30;
        for (int i = 0; i < amountOfUsers; i += batchSize) {
            // Create a batch statement
            BatchStatementBuilder batchBuilder = BatchStatement.builder(BatchType.LOGGED);

            for (int j = i; j < i + batchSize; j++) {
                User user = generateRandomUser();
                UUID id = UUID.randomUUID();
                insertStatement = SimpleStatement.newInstance(cqlInsert, id, user.getUsername(), user.getFirstname(), user.getSecondname(),
                        user.getDateofbirthday(), user.getAge(), user.getPassword());

                // Add the insert statement to the batch
                batchBuilder.addStatement(insertStatement);
            }

            // Execute the batch statement
            BatchStatement batchStatement = batchBuilder.build();
            session.execute(batchStatement);
        }

        Instant end = Instant.now();
        logResult(amountOfUsers, ChronoUnit.SECONDS.between(start, end), "Cassandra");

        // Drop user table if exists
        String cqlDrop = "DROP TABLE IF EXISTS users";
        SimpleStatement dropStatement = SimpleStatement.newInstance(cqlDrop);
        session.execute(dropStatement);

        // Close the session
        session.close();
    }


}
