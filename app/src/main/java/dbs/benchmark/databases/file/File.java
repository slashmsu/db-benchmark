package dbs.benchmark.databases.file;

import dbs.benchmark.User;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Random;

import static dbs.benchmark.databases.shared.Shared.generateRandomUser;
import static dbs.benchmark.databases.shared.Shared.logResult;

public class File {

    public static void insertDataToFile(
            final int amountOfUsers,
            final String fileName
    ) {

        try (BufferedWriter writer = new BufferedWriter(new FileWriter(fileName))) {
            Instant start = Instant.now();

            String userRecordHeader =
                    "id,"
                    + "username,"
                    + "firstname,"
                    + "secondname,"
                    + "dateofbirthday,"
                    + "age,"
                    + "password";

            writer.write(userRecordHeader);
            writer.newLine();  // write new line

            for (int i = 0; i < amountOfUsers; i++) {
                User user = generateRandomUser();

                String userRecord = i + ","
                        + user.getUsername() + ","
                        + user.getFirstname() + ","
                        + user.getSecondname() + ","
                        + user.getDateofbirthday() + ","
                        + user.getAge() + ","
                        + user.getPassword();

                writer.write(userRecord);
                writer.newLine();  // write new line
            }
            Instant end = Instant.now();
            logResult(amountOfUsers, ChronoUnit.SECONDS.between(start, end), "File");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
