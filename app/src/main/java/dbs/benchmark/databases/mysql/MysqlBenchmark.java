package dbs.benchmark.databases.mysql;

import dbs.benchmark.App;
import dbs.benchmark.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.time.Instant;
import java.time.temporal.ChronoUnit;

import static dbs.benchmark.databases.shared.Shared.generateRandomUser;
import static dbs.benchmark.databases.shared.Shared.logResult;

public class MysqlBenchmark {

    private static final Logger logger = LoggerFactory.getLogger(App.class);

    public static void insertDataToMySQL(final int amountOfUsers) {
        String url = "jdbc:mysql://localhost:3306/testdb";
        String userDb = "root";
        String passwordDb = "rootpassword";

        try (Connection connection = DriverManager.getConnection(url, userDb, passwordDb)) {
            try {
                String sqlCreateTable = "CREATE TABLE IF NOT EXISTS users (id INT PRIMARY KEY AUTO_INCREMENT, username VARCHAR(255), firstname VARCHAR(255), secondname VARCHAR(255), dateofbirthday DATE, age INT, userpassword VARCHAR(255))";
                try (PreparedStatement stmt = connection.prepareStatement(sqlCreateTable)) {
                    stmt.execute();
                }

                String sql = "INSERT INTO users (username, firstname, secondname, dateofbirthday, age, userpassword) VALUES (?, ?, ?, ?, ?, ?)";
                try (PreparedStatement stmt = connection.prepareStatement(sql)) {
                    Instant start = Instant.now();
                    for (int i = 0; i < amountOfUsers; i++) {
                        User user = generateRandomUser();
                        stmt.setString(1, user.getUsername());
                        stmt.setString(2, user.getFirstname());
                        stmt.setString(3, user.getSecondname());
                        stmt.setDate(4, java.sql.Date.valueOf(user.getDateofbirthday()));
                        stmt.setInt(5, user.getAge());
                        stmt.setString(6, user.getPassword());
                        stmt.addBatch();
                    }
                    stmt.executeBatch();
                    Instant end = Instant.now();
                    logResult(amountOfUsers, ChronoUnit.SECONDS.between(start, end), "MySQL");
                }
            } catch (SQLException e) {
                logger.error("Error while inserting data to MySQL", e);
            }

            // Drop user table
            try {
                String sql = "DROP TABLE users";
                try (PreparedStatement stmt = connection.prepareStatement(sql)) {
                    stmt.execute();
                }
            } catch (SQLException e) {
                logger.error("Error while dropping table", e);
            }
        } catch (SQLException e) {
            logger.error("Error while connecting to MySQL", e);
        }

    }
}
