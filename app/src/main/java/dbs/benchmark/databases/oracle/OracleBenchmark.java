package dbs.benchmark.databases.oracle;

import dbs.benchmark.User;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.time.Instant;
import java.time.temporal.ChronoUnit;

import static dbs.benchmark.databases.shared.Shared.generateRandomUser;
import static dbs.benchmark.databases.shared.Shared.logResult;

public class OracleBenchmark {

    public static void insertDataToOracle(final int amountOfUsers) throws SQLException {
        String url = "jdbc:oracle:thin:@//localhost:1521/xe";
        String userDb = "system";
        String passwordDb = "oracle";
        try (Connection connection = DriverManager.getConnection(url, userDb, passwordDb)) {
            String sql = "INSERT INTO users (username, firstname, secondname, dateofbirthday, age, userpassword) VALUES (?, ?, ?, ?, ?, ?)";
            try (PreparedStatement stmt = connection.prepareStatement(sql)) {
                Instant start = Instant.now();
                for (int i = 0; i < amountOfUsers; i++) {
                    User user = generateRandomUser();
                    stmt.setString(1, user.getUsername());
                    stmt.setString(2, user.getFirstname());
                    stmt.setString(3, user.getSecondname());
                    stmt.setDate(4, java.sql.Date.valueOf(user.getDateofbirthday()));
                    stmt.setInt(5, user.getAge());
                    stmt.setString(6, user.getPassword());
                    stmt.addBatch();
                }
                stmt.executeBatch();
                Instant end = Instant.now();
                logResult(amountOfUsers, ChronoUnit.SECONDS.between(start, end), "PostgreSQL");
            }
        }
    }
}
