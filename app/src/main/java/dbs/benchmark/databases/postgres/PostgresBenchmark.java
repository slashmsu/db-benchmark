package dbs.benchmark.databases.postgres;

import dbs.benchmark.User;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.time.Instant;
import java.time.temporal.ChronoUnit;

import static dbs.benchmark.databases.shared.Shared.generateRandomUser;
import static dbs.benchmark.databases.shared.Shared.logResult;

public class PostgresBenchmark {

    public static  void insertDataToPostgreSQL(final int amountOfUsers) throws SQLException {
        String url = "jdbc:postgresql://localhost:5432/testdb";
        String userDb = "postgres";
        String passwordDb = "mysecretpassword";

        // Create user table if not exists
        try (Connection connection = DriverManager.getConnection(url, userDb, passwordDb)) {
            String sql = "CREATE TABLE IF NOT EXISTS users (id SERIAL PRIMARY KEY, username VARCHAR(255), firstname VARCHAR(255), secondname VARCHAR(255), dateofbirthday DATE, age INTEGER, userpassword VARCHAR(255))";
            try (PreparedStatement stmt = connection.prepareStatement(sql)) {
                stmt.execute();
            }
        }

        try (Connection connection = DriverManager.getConnection(url, userDb, passwordDb)) {
            String sql = "INSERT INTO users (username, firstname, secondname, dateofbirthday, age, userpassword) VALUES (?, ?, ?, ?, ?, ?)";
            final int batchSize = 10_000_000; // adjust if needed
            try (PreparedStatement stmt = connection.prepareStatement(sql)) {
                Instant start = Instant.now();
                for (int i = 0; i < amountOfUsers; i++) {
                    User user = generateRandomUser();
                    stmt.setString(1, user.getUsername());
                    stmt.setString(2, user.getFirstname());
                    stmt.setString(3, user.getSecondname());
                    stmt.setDate(4, java.sql.Date.valueOf(user.getDateofbirthday()));
                    stmt.setInt(5, user.getAge());
                    stmt.setString(6, user.getPassword());
                    stmt.addBatch();
                    if ((i + 1) % batchSize == 0) {
                        stmt.executeBatch(); // execute every batchSize
                    }
                }
                stmt.executeBatch(); // execute the remaining if any
                Instant end = Instant.now();
                logResult(amountOfUsers, ChronoUnit.SECONDS.between(start, end), "PostgreSQL");
            }
        }

        // Drop user table if exists
        try (Connection connection = DriverManager.getConnection(url, userDb, passwordDb)) {
            String sql = "DROP TABLE IF EXISTS users";
            try (PreparedStatement stmt = connection.prepareStatement(sql)) {
                stmt.execute();
            }
        }
    }


}
