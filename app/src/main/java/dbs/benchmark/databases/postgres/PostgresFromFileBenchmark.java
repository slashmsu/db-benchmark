package dbs.benchmark.databases.postgres;

import org.postgresql.copy.CopyManager;
import org.postgresql.core.BaseConnection;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.time.Instant;
import java.time.temporal.ChronoUnit;

import static dbs.benchmark.databases.shared.Shared.logResult;

public class PostgresFromFileBenchmark {

    public static  void insertDataToPostgreSQLFromFile(
            final int amountOfUsers,
            final String filePath
    ) throws SQLException {
        String url = "jdbc:postgresql://localhost:5432/testdb";
        String userDb = "postgres";
        String passwordDb = "mysecretpassword";

        // Create user table if not exists
        try (Connection connection = DriverManager.getConnection(url, userDb, passwordDb)) {
            String sql = "CREATE TABLE IF NOT EXISTS users (id SERIAL PRIMARY KEY, username VARCHAR(255), firstname VARCHAR(255), secondname VARCHAR(255), dateofbirthday DATE, age INTEGER, userpassword VARCHAR(255))";
            try (PreparedStatement stmt = connection.prepareStatement(sql)) {
                stmt.execute();
            }
        }

        String tableName = "users";

        try (Connection connection = DriverManager.getConnection(url, userDb, passwordDb)) {
            String sql = "COPY " + tableName + " FROM STDIN DELIMITER ',' CSV HEADER";
            BaseConnection pgConnection = connection.unwrap(BaseConnection.class);

            CopyManager copyManager = new CopyManager(pgConnection);
            FileReader fileReader = new FileReader(filePath);

            Instant start = Instant.now();
            copyManager.copyIn(sql, fileReader);
            Instant end = Instant.now();

            logResult(amountOfUsers, ChronoUnit.SECONDS.between(start, end), "Copy to PostgreSQL from file");
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        // Drop user table if exists
        try (Connection connection = DriverManager.getConnection(url, userDb, passwordDb)) {
            String sql = "DROP TABLE IF EXISTS users";
            try (PreparedStatement stmt = connection.prepareStatement(sql)) {
                stmt.execute();
            }
        }

    }
}
