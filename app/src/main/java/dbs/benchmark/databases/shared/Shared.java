package dbs.benchmark.databases.shared;

import dbs.benchmark.App;
import dbs.benchmark.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Random;

public class Shared {
    public static final Logger logger = LoggerFactory.getLogger(App.class);

    public static final int AMOUNT_OF_USERS = 25_000;
    public static final Random random = new Random();
    public static final String ALPHA_NUMERIC_STRING = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz";

    public static User generateRandomUser() {
        String username = randomString(10);
        String firstname = randomString(5);
        String secondname = randomString(7);
        LocalDate dateofbirthday = randomDate();
        int age = (int) ChronoUnit.YEARS.between(dateofbirthday, LocalDate.now());
        String password = randomString(8);

        return new User(username, firstname, secondname, dateofbirthday, age, password);
    }

    public static String randomString(int length) {
        StringBuilder builder = new StringBuilder();
        while (length-- != 0) {
            int character = (int) (Math.random() * ALPHA_NUMERIC_STRING.length());
            builder.append(ALPHA_NUMERIC_STRING.charAt(character));
        }
        return builder.toString();
    }

    public static LocalDate randomDate() {
        long minDay = LocalDate.of(1950, 1, 1).toEpochDay();
        long maxDay = LocalDate.of(2005, 12, 31).toEpochDay();
        long randomDay = minDay + random.nextInt((int) (maxDay - minDay));
        return LocalDate.ofEpochDay(randomDay);
    }

    public static void logResult(int amountOfRecords, long time, String databaseName) {
        logger.info("===========" + databaseName + "====================");
        logger.info("Amount of records: " + amountOfRecords);
        logger.info("Time: " + time + " seconds");
        logger.info("Database: " + databaseName);
        logger.info("Records per second: " + (amountOfRecords / (time / 1000.0)));
        logger.info("=====================================");
    }

    public static void deleteFileByName(String filePathString) {
        Path filePath = Paths.get(filePathString);
        try {
            Files.delete(filePath);
            System.out.println("File deleted successfully.");
        } catch (IOException e) {
            System.out.println("Error occurred while trying to delete the file.");
            e.printStackTrace();
        }

    }

}
